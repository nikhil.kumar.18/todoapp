const button = document.querySelector('#add-list')

const div = document.getElementById('main')

const data = document.querySelector('#task-input')


// get items from local storage 

if (localStorage['todoData']) {
    let arr = JSON.parse(localStorage['todoData'])


    arr.forEach((keys) => {

        let keyArr = Object.keys(keys)

        let tempObj = {}

        tempObj['id'] = keyArr[0]

        tempObj['checked'] = keys[keyArr[1]]

        tempObj['value'] = keys[keyArr[0]]

        const label = document.createElement('label')

        label.className = "child list"

        label.id = tempObj['id']

        const rmvBtn = document.createElement('button')

        const spanData = document.createElement('span')

        spanData.append(tempObj.value)

        spanData.className = "spanText"

        rmvBtn.append("Remove Item")

        const editBtn = document.createElement('button')

        editBtn.append("Edit")

        const check = document.createElement('input')

        check.setAttribute("type", "checkbox")

        check.checked = tempObj['checked']

        label.append(check)

        label.append(spanData)

        label.append(rmvBtn)

        label.append(editBtn)


        div.appendChild(label)

        if (check.checked) {
            spanData.style.textDecoration = "line-through"

        }
        else {
            spanData.style.textDecoration = "none"


        }


        check.addEventListener('click', () => {
            let rmvEleArr = JSON.parse(localStorage['todoData'])
            let index = -1;
            rmvEleArr.forEach((ele, ind) => {
                if (ele[`${label.getAttribute('id')}`]) {
                    index = ind;
                }
            });
            rmvEleArr[index]['checked'] = check.checked
            localStorage['todoData'] = JSON.stringify(rmvEleArr)
            if (check.checked) {
                spanData.style.textDecoration = "line-through"

            }
            else {
                spanData.style.textDecoration = "none"


            }
        })

        rmvBtn.addEventListener('click', () => {
            let rmvEleArr = JSON.parse(localStorage['todoData'])
            let index = -1;
            rmvEleArr.forEach((ele, ind) => {
                if (ele[`${label.getAttribute('id')}`]) {
                    index = ind;
                }
            });

            let tempArr = [...rmvEleArr.slice(0, index), ...rmvEleArr.slice(index + 1)]

            localStorage['todoData'] = JSON.stringify(tempArr)

            label.remove()

        })

        const editBox = document.createElement('input')

        editBox.value = spanData.innerText

        editBtn.addEventListener('click', edit)

        editBox.addEventListener('keypress', (e) => {
            if (e.key == 'Enter') {
                edit()
            }
        })

        function edit() {


            if (editBtn.innerText == 'Edit') {
                label.replaceChild(editBox, spanData)
                editBtn.innerText = "Save"
            }
            else {
                spanData.innerText = editBox.value

                let index = -1;

                let tempArr = JSON.parse(localStorage['todoData'])

                tempArr.forEach((ele, ind) => {
                    let key = Object.keys(ele)
                    if (key[0] == label.getAttribute('id')) {
                        index = ind
                    }
                })

                tempArr[index][`${label.getAttribute('id')}`] = spanData.innerText
                localStorage["todoData"] = JSON.stringify(tempArr)

                label.replaceChild(spanData, editBox)
                editBtn.innerText = "Edit"
            }
        }



    })

}


button.addEventListener('click', addList)

data.addEventListener('keypress', (e) => {
    if (e.key == 'Enter') {
        addList()
    }
})


function addList() {

    const label = document.createElement('label')

    label.className = "child list"

    const randomNum = Math.floor(Math.random() * 90 + 10);

    label.id = `${data.value}${div.childNodes.length * randomNum}`

    const rmvBtn = document.createElement('button')

    const spanData = document.createElement('span')

    const editBtn = document.createElement('button')

    editBtn.append("Edit")

    spanData.append(data.value)

    spanData.className = "spanText"

    rmvBtn.append("Remove Item")

    const check = document.createElement('input')

    check.setAttribute("type", "checkbox")

    label.append(check)

    label.append(spanData)

    label.append(rmvBtn)

    label.append(editBtn)

    if (!localStorage['todoData']) {
        let tempArr = []
        localStorage['todoData'] = JSON.stringify(tempArr);
    }
    let todoArr = JSON.parse(localStorage['todoData'])




    if (data.value) {
        let id = label.getAttribute('id')
        let tempObj = {
            [id]: `${data.value}`,
            "checked": check.checked
        }
        todoArr.push(tempObj)

        localStorage['todoData'] = JSON.stringify(todoArr)

        div.appendChild(label)
    }

    data.value = ""

    check.addEventListener('click', () => {
        let rmvEleArr = JSON.parse(localStorage['todoData'])
        let index = -1;
        rmvEleArr.forEach((ele, ind) => {
            if (ele[`${label.getAttribute('id')}`]) {
                index = ind;
            }
        });
        rmvEleArr[index]['checked'] = check.checked
        localStorage['todoData'] = JSON.stringify(rmvEleArr)

        if (check.checked) {
            spanData.style.textDecoration = "line-through"




        }
        else {
            spanData.style.textDecoration = "none"


        }
    })

    rmvBtn.addEventListener('click', () => {
        let rmvEleArr = JSON.parse(localStorage['todoData'])
        let index = -1;
        rmvEleArr.forEach((ele, ind) => {
            if (ele[`${label.getAttribute('id')}`]) {
                index = ind;
            }
        });

        let tempArr = [...rmvEleArr.slice(0, index), ...rmvEleArr.slice(index + 1)]

        localStorage['todoData'] = JSON.stringify(tempArr)

        label.remove()

    })

    const editBox = document.createElement('input')

    editBox.value = spanData.innerText

    editBtn.addEventListener('click', edit)

    editBox.addEventListener('keypress', (e) => {
        if (e.key == 'Enter') {
            edit()
        }
    })

    function edit() {


        if (editBtn.innerText == 'Edit') {
            label.replaceChild(editBox, spanData)
            editBtn.innerText = "Save"
        }
        else {
            spanData.innerText = editBox.value

            let index = -1;

            let tempArr = JSON.parse(localStorage['todoData'])

            tempArr.forEach((ele, ind) => {
                let key = Object.keys(ele)
                if (key[0] == label.getAttribute('id')) {
                    index = ind
                }
            })

            tempArr[index][`${label.getAttribute('id')}`] = spanData.innerText
            localStorage["todoData"] = JSON.stringify(tempArr)

            label.replaceChild(spanData, editBox)
            editBtn.innerText = "Edit"
        }
    }
}
